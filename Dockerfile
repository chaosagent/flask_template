FROM python:3.5.2

RUN apt-get update && apt-get install -y netcat

RUN mkdir /app
COPY requirements.txt /app/
WORKDIR /app
RUN pip3 install -r requirements.txt
COPY . /app/

CMD ["bash", "-c", "bash wait-for-db.sh && gunicorn --worker-class eventlet --bind 0.0.0.0:80 -w 1 app:app"]
